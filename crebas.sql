/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     15/07/2021 19:01:54                          */
/*==============================================================*/


drop index RELATIONSHIP_7_FK;

drop index DEPARTAMENTOCONTABILIDAD_PK;

drop table DEPARTAMENTOCONTABILIDAD;

drop index RELATIONSHIP_9_FK;

drop index EMPLEADO_PK;

drop table EMPLEADO;

drop index RELATIONSHIP_6_FK;

drop index ESTANCIA_PK;

drop table ESTANCIA;

drop index RELATIONSHIP_10_FK;

drop index HABITACION_PK;

drop table HABITACION;

drop index RELATIONSHIP_5_FK;

drop index HUESPED_PK;

drop table HUESPED;

drop index MANTENIMIENTO_PK;

drop table MANTENIMIENTO;

drop index RELATIONSHIP_8_FK;

drop index PROVEEDOR_PK;

drop table PROVEEDOR;

drop index RELATIONSHIP_2_FK;

drop index RELATIONSHIP_1_FK;

drop index RELATIONSHIP_1_PK;

drop table RELATIONSHIP_1;

drop index RELATIONSHIP_4_FK;

drop index RELATIONSHIP_3_FK;

drop index RELATIONSHIP_3_PK;

drop table RELATIONSHIP_3;

drop index SERVICIOS_PK;

drop table SERVICIOS;

/*==============================================================*/
/* Table: DEPARTAMENTOCONTABILIDAD                              */
/*==============================================================*/
create table DEPARTAMENTOCONTABILIDAD (
   IDFACTURA            VARCHAR(23)          not null,
   IDPROVEEDOR          INT4                 null,
   TIPOFACTURA          VARCHAR(23)          null,
   constraint PK_DEPARTAMENTOCONTABILIDAD primary key (IDFACTURA)
);

/*==============================================================*/
/* Index: DEPARTAMENTOCONTABILIDAD_PK                           */
/*==============================================================*/
create unique index DEPARTAMENTOCONTABILIDAD_PK on DEPARTAMENTOCONTABILIDAD (
IDFACTURA
);

/*==============================================================*/
/* Index: RELATIONSHIP_7_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_7_FK on DEPARTAMENTOCONTABILIDAD (
IDPROVEEDOR
);

/*==============================================================*/
/* Table: EMPLEADO                                              */
/*==============================================================*/
create table EMPLEADO (
   IDEMPLEADO           INT4                 not null,
   IDMANTENIMIENTO      INT4                 null,
   NOMBREEMPLEADO       VARCHAR(23)          null,
   TIPOEMPLEADO         VARCHAR(23)          null,
   CEDULAEMPLEADO       VARCHAR(23)          null,
   EDADEMPLEADO         VARCHAR(23)          null,
   DIRECCIONEMPLEADO    VARCHAR(23)          null,
   GENEROEMPLEADO       VARCHAR(23)          null,
   TELEFONOEMPLEADO     VARCHAR(23)          null,
   constraint PK_EMPLEADO primary key (IDEMPLEADO)
);

/*==============================================================*/
/* Index: EMPLEADO_PK                                           */
/*==============================================================*/
create unique index EMPLEADO_PK on EMPLEADO (
IDEMPLEADO
);

/*==============================================================*/
/* Index: RELATIONSHIP_9_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_9_FK on EMPLEADO (
IDMANTENIMIENTO
);

/*==============================================================*/
/* Table: ESTANCIA                                              */
/*==============================================================*/
create table ESTANCIA (
   IDALOJAMIENTO        INT4                 not null,
   IDFACTURA            VARCHAR(23)          null,
   FECHAINICIOALOJAMIENTO VARCHAR(23)          null,
   FECHAFINALALOJAMIENTO VARCHAR(23)          null,
   FORMADEPAGO          VARCHAR(23)          null,
   PROMOCIONNES         VARCHAR(23)          null,
   ID_HUESPED           INT4                 null,
   ID_HABITACION        INT4                 null,
   constraint PK_ESTANCIA primary key (IDALOJAMIENTO)
);

/*==============================================================*/
/* Index: ESTANCIA_PK                                           */
/*==============================================================*/
create unique index ESTANCIA_PK on ESTANCIA (
IDALOJAMIENTO
);

/*==============================================================*/
/* Index: RELATIONSHIP_6_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_6_FK on ESTANCIA (
IDFACTURA
);

/*==============================================================*/
/* Table: HABITACION                                            */
/*==============================================================*/
create table HABITACION (
   IDHABITACION         INT4                 not null,
   IDALOJAMIENTO        INT4                 null,
   TIPOHABITACION       VARCHAR(23)          null,
   NUMEROHABITACION     VARCHAR(23)          null,
   constraint PK_HABITACION primary key (IDHABITACION)
);

/*==============================================================*/
/* Index: HABITACION_PK                                         */
/*==============================================================*/
create unique index HABITACION_PK on HABITACION (
IDHABITACION
);

/*==============================================================*/
/* Index: RELATIONSHIP_10_FK                                    */
/*==============================================================*/
create  index RELATIONSHIP_10_FK on HABITACION (
IDALOJAMIENTO
);

/*==============================================================*/
/* Table: HUESPED                                               */
/*==============================================================*/
create table HUESPED (
   IDHUESPED            INT4                 not null,
   IDALOJAMIENTO        INT4                 null,
   NOMBREHUESPED        VARCHAR(23)          null,
   GENEROHUESPED        VARCHAR(23)          null,
   CEDULAHUESPED        VARCHAR(23)          null,
   TELEFONOHUESPED      VARCHAR(23)          null,
   DIRECCIONHUESPED     VARCHAR(23)          null,
   EMAILHUESPED         VARCHAR(23)          null,
   constraint PK_HUESPED primary key (IDHUESPED)
);

/*==============================================================*/
/* Index: HUESPED_PK                                            */
/*==============================================================*/
create unique index HUESPED_PK on HUESPED (
IDHUESPED
);

/*==============================================================*/
/* Index: RELATIONSHIP_5_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_5_FK on HUESPED (
IDALOJAMIENTO
);

/*==============================================================*/
/* Table: MANTENIMIENTO                                         */
/*==============================================================*/
create table MANTENIMIENTO (
   IDMANTENIMIENTO      INT4                 not null,
   CANTIDADMANTENIMIENTO VARCHAR(23)          null,
   FECHAMANTENIMIENTO   VARCHAR(23)          null,
   TIPOMANTENIMIENTO    VARCHAR(23)          null,
   constraint PK_MANTENIMIENTO primary key (IDMANTENIMIENTO)
);

/*==============================================================*/
/* Index: MANTENIMIENTO_PK                                      */
/*==============================================================*/
create unique index MANTENIMIENTO_PK on MANTENIMIENTO (
IDMANTENIMIENTO
);

/*==============================================================*/
/* Table: PROVEEDOR                                             */
/*==============================================================*/
create table PROVEEDOR (
   IDPROVEEDOR          INT4                 not null,
   IDALOJAMIENTO        INT4                 null,
   TIPOPROVEEDOR        VARCHAR(23)          null,
   TELEFONOPROVEEDOR    VARCHAR(23)          null,
   FACTURAPROVEEDOR     VARCHAR(23)          null,
   PAGINAWEB            VARCHAR(24)          null,
   DIRECCIONPROVEEDOR   VARCHAR(23)          null,
   constraint PK_PROVEEDOR primary key (IDPROVEEDOR)
);

/*==============================================================*/
/* Index: PROVEEDOR_PK                                          */
/*==============================================================*/
create unique index PROVEEDOR_PK on PROVEEDOR (
IDPROVEEDOR
);

/*==============================================================*/
/* Index: RELATIONSHIP_8_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_8_FK on PROVEEDOR (
IDALOJAMIENTO
);

/*==============================================================*/
/* Table: RELATIONSHIP_1                                        */
/*==============================================================*/
create table RELATIONSHIP_1 (
   ID_SERVICIO          INT4                 not null,
   IDEMPLEADO           INT4                 not null,
   constraint PK_RELATIONSHIP_1 primary key (ID_SERVICIO, IDEMPLEADO)
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_PK                                     */
/*==============================================================*/
create unique index RELATIONSHIP_1_PK on RELATIONSHIP_1 (
ID_SERVICIO,
IDEMPLEADO
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on RELATIONSHIP_1 (
ID_SERVICIO
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on RELATIONSHIP_1 (
IDEMPLEADO
);

/*==============================================================*/
/* Table: RELATIONSHIP_3                                        */
/*==============================================================*/
create table RELATIONSHIP_3 (
   IDHABITACION         INT4                 not null,
   IDMANTENIMIENTO      INT4                 not null,
   constraint PK_RELATIONSHIP_3 primary key (IDHABITACION, IDMANTENIMIENTO)
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_PK                                     */
/*==============================================================*/
create unique index RELATIONSHIP_3_PK on RELATIONSHIP_3 (
IDHABITACION,
IDMANTENIMIENTO
);

/*==============================================================*/
/* Index: RELATIONSHIP_3_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_3_FK on RELATIONSHIP_3 (
IDHABITACION
);

/*==============================================================*/
/* Index: RELATIONSHIP_4_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_4_FK on RELATIONSHIP_3 (
IDMANTENIMIENTO
);

/*==============================================================*/
/* Table: SERVICIOS                                             */
/*==============================================================*/
create table SERVICIOS (
   ID_SERVICIO          INT4                 not null,
   TIPODEPAGOSERVICIO   VARCHAR(23)          null,
   TIPOSERVICIO         VARCHAR(23)          null,
   FACTURASERVICO       VARCHAR(23)          null,
   constraint PK_SERVICIOS primary key (ID_SERVICIO)
);

/*==============================================================*/
/* Index: SERVICIOS_PK                                          */
/*==============================================================*/
create unique index SERVICIOS_PK on SERVICIOS (
ID_SERVICIO
);

alter table DEPARTAMENTOCONTABILIDAD
   add constraint FK_DEPARTAM_RELATIONS_PROVEEDO foreign key (IDPROVEEDOR)
      references PROVEEDOR (IDPROVEEDOR)
      on delete restrict on update restrict;

alter table EMPLEADO
   add constraint FK_EMPLEADO_RELATIONS_MANTENIM foreign key (IDMANTENIMIENTO)
      references MANTENIMIENTO (IDMANTENIMIENTO)
      on delete restrict on update restrict;

alter table ESTANCIA
   add constraint FK_ESTANCIA_RELATIONS_DEPARTAM foreign key (IDFACTURA)
      references DEPARTAMENTOCONTABILIDAD (IDFACTURA)
      on delete restrict on update restrict;

alter table HABITACION
   add constraint FK_HABITACI_RELATIONS_ESTANCIA foreign key (IDALOJAMIENTO)
      references ESTANCIA (IDALOJAMIENTO)
      on delete restrict on update restrict;

alter table HUESPED
   add constraint FK_HUESPED_RELATIONS_ESTANCIA foreign key (IDALOJAMIENTO)
      references ESTANCIA (IDALOJAMIENTO)
      on delete restrict on update restrict;

alter table PROVEEDOR
   add constraint FK_PROVEEDO_RELATIONS_ESTANCIA foreign key (IDALOJAMIENTO)
      references ESTANCIA (IDALOJAMIENTO)
      on delete restrict on update restrict;

alter table RELATIONSHIP_1
   add constraint FK_RELATION_RELATIONS_SERVICIO foreign key (ID_SERVICIO)
      references SERVICIOS (ID_SERVICIO)
      on delete restrict on update restrict;

alter table RELATIONSHIP_1
   add constraint FK_RELATION_RELATIONS_EMPLEADO foreign key (IDEMPLEADO)
      references EMPLEADO (IDEMPLEADO)
      on delete restrict on update restrict;

alter table RELATIONSHIP_3
   add constraint FK_RELATION_RELATIONS_HABITACI foreign key (IDHABITACION)
      references HABITACION (IDHABITACION)
      on delete restrict on update restrict;

alter table RELATIONSHIP_3
   add constraint FK_RELATION_RELATIONS_MANTENIM foreign key (IDMANTENIMIENTO)
      references MANTENIMIENTO (IDMANTENIMIENTO)
      on delete restrict on update restrict;

